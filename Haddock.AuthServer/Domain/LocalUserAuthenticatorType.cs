﻿namespace Haddock.AuthServer.Domain
{
    public enum LocalUserAuthenticatorType
    {
        Default = 0,
        Ldap = 1,
        Enosis = 2,
    }
}