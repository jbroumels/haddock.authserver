﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Diagnostics.CodeAnalysis;

namespace Haddock.AuthServer.Domain.Repository.Mapping
{
    [ExcludeFromCodeCoverage] // database setup
    public class SimpleClientMapping : EntityTypeConfiguration<SimpleClient>
    {
        public SimpleClientMapping()
        {
            HasKey(q => q.Id);
            Property(q => q.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            ToTable("Clients");
        }
    }
}
