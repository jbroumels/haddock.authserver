﻿using System.Data.Entity;
using Enosis.Common.Dal;

namespace Haddock.AuthServer.Domain.Repository
{
    public interface IAuthServerContext : IContext
    {
        IDbSet<SimpleClient> Clients { get; set; }
        IDbSet<ClientCustomLocalUserAuthentication> ClientCustomLocalUserAuthentications { get; set; } 
    }
}