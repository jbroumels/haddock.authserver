﻿using System.Data.Entity;
using Enosis.Common.Dal;
using Haddock.AuthServer.Domain.Repository.Mapping;

namespace Haddock.AuthServer.Domain.Repository
{
    public class AuthServerContext : ContextBase<AuthServerContext>, IAuthServerContext
    {
        public AuthServerContext()
            : base("AuthServer")
        {
            
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            ApplyContextSpecificMappings(modelBuilder);
        }

        private static void ApplyContextSpecificMappings(DbModelBuilder modelBuilder)
        {
            // create specific mappings here
            modelBuilder.Configurations.Add(new SimpleClientMapping());
            modelBuilder.Configurations.Add(new ClientCustomLocalUserAuthenticationMapping());
        }

        public IDbSet<SimpleClient> Clients { get; set; }
        public IDbSet<ClientCustomLocalUserAuthentication> ClientCustomLocalUserAuthentications { get; set; } 
    }
}
