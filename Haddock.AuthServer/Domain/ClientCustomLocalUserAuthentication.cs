﻿namespace Haddock.AuthServer.Domain
{
    public class ClientCustomLocalUserAuthentication
    {
        public int Id { get; set; }
        public string ClientId { get; set; }
        public LocalUserAuthenticatorType LocalUserAuthenticatorType { get; set; }
    }
}