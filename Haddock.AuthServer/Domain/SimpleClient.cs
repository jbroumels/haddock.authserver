﻿namespace Haddock.AuthServer.Domain
{
    public class SimpleClient
    {
        public int Id { get; set; }

        public string ClientId { get; set; }
        public string ClientName { get; set; }

    }
}
