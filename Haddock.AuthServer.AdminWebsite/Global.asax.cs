﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.Http;
using Enosis.Common.Api.Startup;
using Enosis.Common.Ioc;
using Enosis.Common.Ioc.Simple;
using Enosis.Common.Logging;
using Haddock.AuthServer.Shared;
using Haddock.AuthServer.Shared.IoC;
using Haddock.AuthServer.Shared.MembershipReboot;
using IdentityManager.Configuration;
using Microsoft.Practices.Unity;
using Owin;
using Unity.WebApi;
using UnityConfiguration;

namespace Haddock.AuthServer.AdminWebsite
{
    public class Global : System.Web.HttpApplication
    {
        private ISimpleResolver resolver;

        protected void Application_Start(object sender, EventArgs e)
        {
            LoggerGlobalContext.SetGlobalProperties(new Dictionary<string, string>()
            {
                {EnosisStandardContextKeys.ApplicationGroup.ToString(), "Haddock"},
                {EnosisStandardContextKeys.ApplicationName.ToString(), ConfigurationManager.AppSettings["ApplicationName"]},
            });

            StartUnity();

            LoggingExtensions.Logging.Log.InitializeWith<AuthServerLogWrapper>();

            GlobalConfiguration.Configure(WebApiConfig.Register);

            StartupConfig.Run(GlobalConfiguration.Configuration, new StartupSettings
            {
                ApplyDefaultFormatters = true,
                EnableCors = true, // TODO: handle custom CORS, because this one is allowing all hosts
                EnableCorrelationActionFilter = true,
                EnableDefaultRequestResponseLogging = true,
            }, resolver);

            GlobalConfiguration.Configuration.EnsureInitialized();
        }


        private void StartUnity()
        {
            if (resolver == null)
            {

                IUnityContainer container = IocConfig.BuildUnityContainer();
                container.Configure(x => x.AddRegistry<HaddockRegistry>());

                resolver = container.Resolve<ISimpleResolver>();

                var dependencyResolver = new UnityDependencyResolver(container);

                GlobalConfiguration.Configuration.DependencyResolver = dependencyResolver;
            }

        }

        public void Configuration(IAppBuilder app)
        {
            StartUnity();

            app.Map("", adminApp =>
                        {
                            IdentityManagerServiceFactory factory = new IdentityManagerServiceFactory();
                            factory.Configure();

                            adminApp.UseIdentityManager(new IdentityManagerOptions()
                                                        {
                                                            Factory = factory,
                                                            DisableUserInterface = false,
                                                            SecurityConfiguration = new LocalhostSecurityConfiguration()
                                                                                    {
                                                                                        RequireSsl = false
                                                                                    }
                                                        });
                        });
        }


        protected void Application_End(object sender, EventArgs e)
        {
            if (resolver != null)
            {
                ILogger logger = resolver.Resolve<ILogger>();
                logger.ShutdownLoggingSystem(true);
            }
        }
    }
}