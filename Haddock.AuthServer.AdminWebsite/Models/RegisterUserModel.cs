﻿using System;

namespace Haddock.AuthServer.AdminWebsite.Models
{
    public class RegisterUserModel
    {
        public Guid? Id { get; set; }

        public string Username { get; set; }
        public string Password { get; set; }
        public string EmailAddress { get; set; }
    }
}