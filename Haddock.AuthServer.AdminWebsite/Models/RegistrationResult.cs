﻿using System;

namespace Haddock.AuthServer.AdminWebsite.Models
{

    public class RegistrationResult
    {
        public Guid Id { get; set; }
    }
}