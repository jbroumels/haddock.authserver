﻿using System.IO;
using System.Security.Cryptography.X509Certificates;

namespace Haddock.AuthServer.AdminWebsite
{
    static class Certificate
    {
        public static X509Certificate2 Get(string certificateName, string certificatePassword)
        {
            var assembly = typeof(Certificate).Assembly;
            using (var stream = assembly.GetManifestResourceStream(string.Format("Haddock.AuthServer.AdminWebsite.{0}.pfx", certificateName)))
            {
                return new X509Certificate2(ReadStream(stream), certificatePassword);
            }
        }

        private static byte[] ReadStream(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }
    }
}