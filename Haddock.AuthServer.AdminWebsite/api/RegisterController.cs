﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Http;
using Haddock.AuthServer.AdminWebsite.Models;
using Haddock.AuthServer.Shared.MembershipReboot;

namespace Haddock.AuthServer.AdminWebsite.api
{
    public class RegisterController : ApiController
    {
        CustomUserAccountService userAccountService;

        public RegisterController(CustomUserAccountService userAccountService)
        {
            this.userAccountService = userAccountService;
        }

        [HttpPost]
        [Route("api/account/create")]
        public IHttpActionResult RegisterAccount(RegisterUserModel model)
        {
            CustomUser account;

            try
            {
                account = this.userAccountService.CreateAccount(model.Username, model.Password, model.EmailAddress, model.Id);
            }
            catch (ValidationException vex)
            {
                throw vex;
            }

            return Ok(new RegistrationResult()
                      {
                          Id = account.ID
                      });
        }
    }

}
