﻿using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using IdentityServer3.Core.Models;

namespace Haddock.AuthServer.Authenticator.Ldap
{
    public class LdapUserAuthenticator : ILdapUserAuthenticator
    {
        private readonly LdapServerSettings ldapServerSettings;

        public LdapUserAuthenticator(LdapServerSettings ldapServerSettings)
        {
            this.ldapServerSettings = ldapServerSettings;
        }

        public Task PreAuthenticateAsync(PreAuthenticationContext context)
        {
            throw new System.NotImplementedException();
        }

        public Task AuthenticateLocalAsync(LocalAuthenticationContext context)
        {
            using (var principalContext = new PrincipalContext(ContextType.Domain, ldapServerSettings.Url, ldapServerSettings.UserName, ldapServerSettings.Password))
            {
                var result = LdapLogin(context.UserName, context.Password, principalContext);
                if (result == null)
                {
                    return Task.FromResult(new AuthenticateResult(string.Format("Login for user {0} failed", context.UserName)));
                }

                var claims = result.GetAuthorizationGroups().Select(x => new Claim("role", x.SamAccountName));

                context.AuthenticateResult = new AuthenticateResult(context.SignInMessage.ClientId, result.SamAccountName, claims);

                return Task.FromResult(0);
            }
        }

        public Task AuthenticateExternalAsync(ExternalAuthenticationContext context)
        {
            throw new System.NotImplementedException();
        }

        public Task PostAuthenticateAsync(PostAuthenticationContext context)
        {
            throw new System.NotImplementedException();
        }

        public Task SignOutAsync(SignOutContext context)
        {
            throw new System.NotImplementedException();
        }

        public Task GetProfileDataAsync(ProfileDataRequestContext context)
        {
            throw new System.NotImplementedException();
        }

        public Task IsActiveAsync(IsActiveContext context)
        {
            throw new System.NotImplementedException();
        }

        private UserPrincipal LdapLogin(string user, string pwd, PrincipalContext context)
        {
            if (!context.ValidateCredentials(user, pwd))
            {
                return null;
            }

            using (var qbeUser = new UserPrincipal(context) { SamAccountName = user })
            using (var srch = new PrincipalSearcher(qbeUser))
            {
                return srch.FindOne() as UserPrincipal;
            }

        }
    }
}