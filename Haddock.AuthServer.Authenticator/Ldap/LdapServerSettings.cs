using System.DirectoryServices.AccountManagement;
using Enosis.Common.Settings;

namespace Haddock.AuthServer.Authenticator.Ldap
{
    public class LdapServerSettings
    {
        private readonly ISettingsProvider settingsProvider;

        public LdapServerSettings(ISettingsProvider settingsProvider)
        {
            this.settingsProvider = settingsProvider;
        }

        public string Url
        {
            get { return settingsProvider.GetSetting<string>("Ldap.Url"); }
        }

        public string UserName
        {
            get { return settingsProvider.GetSetting<string>("Ldap.Username"); }
        }

        public string Password
        {
            get { return settingsProvider.GetSetting<string>("Ldap.Password"); }
        }
    }
}