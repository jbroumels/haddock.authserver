﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using BrockAllen.MembershipReboot;
using Haddock.AuthServer.Authenticator.Default;
using Haddock.AuthServer.Shared.MembershipReboot;
using IdentityServer3.Core.Extensions;
using IdentityServer3.Core.Models;

namespace Haddock.AuthServer.Authenticator.Enosis
{
    public class EnosisUserAuthenticator : DefaultUserAuthenticator , IEnosisUserAuthenticator
    {
        public EnosisUserAuthenticator(CustomUserAccountService userAccountService) 
            : base(userAccountService)
        {
        }


        public async new Task AuthenticateLocalAsync(LocalAuthenticationContext context)
        {
            // could go to another datasource here, eg. store password in Enosis system
            await base.AuthenticateLocalAsync(context);
        }

        protected override IEnumerable<Claim> GetClaimsFromAccount(CustomUser account)
        {
            List<Claim> claims = base.GetClaimsFromAccount(account).ToList();

            claims.Add(GetCustomerIdClaim(account));

            return claims;
        }

        private Claim GetCustomerIdClaim(CustomUser account)
        {
            return new Claim("customerid", account.Username);
        }
    }
}
