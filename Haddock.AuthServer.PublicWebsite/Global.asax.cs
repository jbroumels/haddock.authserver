﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.Http;
using System.Web.Security;
using System.Web.SessionState;
using Enosis.Common.Api.Startup;
using Enosis.Common.Ioc;
using Enosis.Common.Ioc.Simple;
using Enosis.Common.Logging;
using Haddock.AuthServer.Domain.Repository;
using Haddock.AuthServer.PublicWebsite.App_Start;
using Haddock.AuthServer.Shared;
using Haddock.AuthServer.Shared.IoC;
using IdentityServer3.Core.Configuration;
using IdentityServer3.Core.Events;
using IdentityServer3.Core.Logging;
using IdentityServer3.Core.Services;
using IdentityServer3.Core.Services.Default;
using IdentityServer3.EntityFramework;
using Microsoft.Practices.Unity;
using Owin;
using Unity.WebApi;
using UnityConfiguration;

namespace Haddock.AuthServer.PublicWebsite
{
    public class Global : System.Web.HttpApplication
    {
        private ISimpleResolver resolver;

        protected void Application_Start(object sender, EventArgs e)
        {
            LoggerGlobalContext.SetGlobalProperties(new Dictionary<string, string>()
            {
                {EnosisStandardContextKeys.ApplicationGroup.ToString(), "Haddock"},
                {EnosisStandardContextKeys.ApplicationName.ToString(), ConfigurationManager.AppSettings["ApplicationName"]},
            });

            StartUnity();
            
            LoggingExtensions.Logging.Log.InitializeWith<AuthServerLogWrapper>();

            GlobalConfiguration.Configure(WebApiConfig.Register);

            StartupConfig.Run(GlobalConfiguration.Configuration, new StartupSettings
            {
                ApplyDefaultFormatters = true,
                EnableCors = true, // TODO: handle custom CORS, because this one is allowing all hosts
                EnableCorrelationActionFilter = true,
                EnableDefaultRequestResponseLogging = true,
            }, resolver);

            GlobalConfiguration.Configuration.EnsureInitialized();
        }

        private void StartUnity()
        {
            if (resolver == null)
            {
                IocConfig.UseSetterInjectionExtension = true; // used to inject ILogger on LogWrapper

                IUnityContainer container = IocConfig.BuildUnityContainer();
                container.Configure(x => x.AddRegistry<HaddockRegistry>());

                resolver = container.Resolve<ISimpleResolver>();

                var dependencyResolver = new UnityDependencyResolver(container);

                GlobalConfiguration.Configuration.DependencyResolver = dependencyResolver;
            }
            
        }

        public void Configuration(IAppBuilder app)
        {
            StartUnity();

            HaddockUserService userService = resolver.Resolve<HaddockUserService>();

            IdentityServerServiceFactory idServFactory = new IdentityServerServiceFactory()
            {
                UserService = new Registration<IUserService>(userService),
                CorsPolicyService = new Registration<ICorsPolicyService>(new DefaultCorsPolicyService { AllowAll = true }),
            };


            var efConfig = new EntityFrameworkServiceOptions { ConnectionString = "AuthServer" };

            idServFactory.RegisterConfigurationServices(efConfig);
            idServFactory.RegisterOperationalServices(efConfig);
            idServFactory.RegisterClientStore(efConfig);
            idServFactory.RegisterScopeStore(efConfig);


            X509Certificate2 primaryCertificate = Certificate.Get(ConfigurationManager.AppSettings["AuthServer.SigningCertificateFriendlyName"], ConfigurationManager.AppSettings["AuthServer.SigningCertificatePassword"]);
            X509Certificate2 secondaryCertificate = null;
            if (ConfigurationManager.AppSettings["AuthServer.SecondarySigningCertificateFriendlyName"] != null)
            {
                secondaryCertificate = Certificate.Get(ConfigurationManager.AppSettings["AuthServer.SecondarySigningCertificateFriendlyName"], ConfigurationManager.AppSettings["AuthServer.SecondarySigningCertificatePassword"]);
            }

            string publicOriginUrl = ConfigurationManager.AppSettings["AuthServer.PublicOrigin"];

            string authServerName = ConfigurationManager.AppSettings["AuthServer.Name"];

            app.UseIdentityServer(new IdentityServerOptions()
            {
                SiteName = authServerName,
                RequireSsl = false,
                Factory = idServFactory,
                EnableWelcomePage = true,
                PublicOrigin = publicOriginUrl,
                SigningCertificate = primaryCertificate,
                SecondarySigningCertificate = secondaryCertificate,
                LoggingOptions = new LoggingOptions()
                                 {
                                     EnableHttpLogging = true,
                                     EnableKatanaLogging = true,
                                     EnableWebApiDiagnostics = true,
                                     WebApiDiagnosticsIsVerbose = true
                                 }
            });
        }

        protected void Application_End(object sender, EventArgs e)
        {
            if (resolver != null)
            {
                ILogger logger = resolver.Resolve<ILogger>();
                logger.ShutdownLoggingSystem(true);
            }
        }
    }
}