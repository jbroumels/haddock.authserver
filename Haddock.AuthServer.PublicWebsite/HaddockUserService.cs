using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Enosis.Common.Dal;
using Enosis.Common.Ioc.Simple;
using Haddock.AuthServer.Authenticator;
using Haddock.AuthServer.Authenticator.Default;
using Haddock.AuthServer.Authenticator.Enosis;
using Haddock.AuthServer.Authenticator.Ldap;
using Haddock.AuthServer.Domain;
using Haddock.AuthServer.Domain.Repository;
using IdentityServer3.Core.Models;
using IdentityServer3.Core.Services.Default;

namespace Haddock.AuthServer.PublicWebsite
{
    public class HaddockUserService : UserServiceBase
    {
        private readonly IContextFactory contextFactory;
        private readonly ISimpleResolver resolver;

        public HaddockUserService(IContextFactory contextFactory, ISimpleResolver resolver)
        {
            this.contextFactory = contextFactory;
            this.resolver = resolver;
        }

        public async override Task AuthenticateLocalAsync(LocalAuthenticationContext context)
        {
            IUserAuthenticator userAuthenticator = null;
            using (IAuthServerContext authServerContext = contextFactory.Construct<IAuthServerContext>())
            {
                // find the client
                var methodToAuthenticate = await authServerContext.ClientCustomLocalUserAuthentications.SingleOrDefaultAsync(it => it.ClientId == context.SignInMessage.ClientId);

                if (methodToAuthenticate == null)
                {
                    context.AuthenticateResult = new AuthenticateResult(string.Format("No User Authenticator defined for client {0}", context.SignInMessage.ClientId));
                    return;
                }

                // do some Storage determination here
                switch (methodToAuthenticate.LocalUserAuthenticatorType)
                {
                    case LocalUserAuthenticatorType.Enosis:
                    {
                        userAuthenticator = resolver.Resolve<IEnosisUserAuthenticator>();
                        break;
                    }
                    case LocalUserAuthenticatorType.Ldap:
                    {
                        userAuthenticator = resolver.Resolve<ILdapUserAuthenticator>();
                        break;
                    }
                    case LocalUserAuthenticatorType.Default:
                    {
                        userAuthenticator = resolver.Resolve<IDefaultUserAuthenticator>();
                        break;
                    }
                    default:
                    {
                        context.AuthenticateResult = new AuthenticateResult(string.Format("No implementation found for LocalUserAuthenticatorType {0}", methodToAuthenticate.LocalUserAuthenticatorType));
                        return;
                    }
                }

            }

            await userAuthenticator.AuthenticateLocalAsync(context);
        }
    }

}