﻿using System.Collections.Generic;
using IdentityServer3.Core;
using IdentityServer3.Core.Models;

namespace Haddock.AuthServer.Seeding.Config
{
    public class Clients
    {
        public static List<Client> Get()
        {
            return new List<Client>
            {
                new Client
                {
                    ClientName = "MyAccount",
                    Enabled = true,

                    ClientId = "MyAccount",
                    ClientSecrets = new List<Secret>
                    { 
                        new Secret("MyAccountPassword".Sha256())
                    },

                    Flow = Flows.Implicit,
                    
                    AllowedScopes = new List<string>
                    {
                        Constants.StandardScopes.OpenId,
                        Constants.StandardScopes.Profile,
                        Constants.StandardScopes.Email,
                        Constants.StandardScopes.Roles,
                        Constants.StandardScopes.OfflineAccess,
                        "read",
                        "write",
                        "MyAccountApi"
                    },
                    
                    ClientUri = "http://localhost/enosis.myaccount.testclient/",
                    LogoUri = "",

                    RequireConsent = false,
                    AllowRememberConsent = true,

                    RedirectUris = new List<string>
                    {
                        // "simple JS client"
                        "http://localhost/enosis.myaccount.testclient/",
                    },

                    PostLogoutRedirectUris = new List<string>
                    {
                        "http://localhost/enosis.myaccount.testclient/",
                    },

                    AllowedCorsOrigins = new List<string>{
                        "http://localhost/enosis.myaccount.testclient/"
                    },
                    
                    IdentityTokenLifetime = 360,
                    AccessTokenLifetime = 3600,
                    AccessTokenType = AccessTokenType.Reference,
                    RefreshTokenUsage = TokenUsage.OneTimeOnly,
                    
                },
                new Client
                {
                    ClientName = "TestClientResourceOwnerFlow",
                    Enabled = true,

                    ClientId = "TestClientResourceOwnerFlow",
                    ClientSecrets = new List<Secret>
                    { 
                        new Secret("TestClientResourceOwnerFlowPassword".Sha256())
                    },

                    Flow = Flows.ResourceOwner,
                    
                    AllowedScopes = new List<string>
                    {
                        Constants.StandardScopes.OpenId,
                        Constants.StandardScopes.Profile,
                        Constants.StandardScopes.Email,
                        Constants.StandardScopes.Roles,
                        Constants.StandardScopes.OfflineAccess,
                        "read",
                        "write"
                    },
                    
                    ClientUri = "",
                    LogoUri = "",

                    RequireConsent = false,
                    AllowRememberConsent = true,

                    IdentityTokenLifetime = 360,
                    AccessTokenLifetime = 3600,
                    AccessTokenType = AccessTokenType.Reference,
                    RefreshTokenUsage = TokenUsage.OneTimeOnly,
                },
            };
        }
    }
}
