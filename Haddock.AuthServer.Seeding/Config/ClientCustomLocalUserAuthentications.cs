﻿using System.Collections.Generic;
using Haddock.AuthServer.Domain;

namespace Haddock.AuthServer.Seeding.Config
{
    class ClientCustomLocalUserAuthentications
    {
        public static IEnumerable<ClientCustomLocalUserAuthentication> Get()
        {
            return new ClientCustomLocalUserAuthentication[]
            {
                new ClientCustomLocalUserAuthentication
                {
                    ClientId = "MyAccount",
                    LocalUserAuthenticatorType = LocalUserAuthenticatorType.Enosis
                },
                new ClientCustomLocalUserAuthentication
                {
                    ClientId = "TestClientResourceOwnerFlow",
                    LocalUserAuthenticatorType = LocalUserAuthenticatorType.Default
                }
             };
        }
    }
}
