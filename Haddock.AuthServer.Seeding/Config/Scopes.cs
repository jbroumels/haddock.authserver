﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IdentityServer3.Core.Models;

namespace Haddock.AuthServer.Seeding.Config
{
    public class Scopes
    {
        public static IEnumerable<Scope> Get()
        {
            return new Scope[]
            {
                StandardScopes.OpenId,
                StandardScopes.Profile,
                StandardScopes.Email,
                StandardScopes.OfflineAccess,
                new Scope
                {
                    Name = "read",
                    DisplayName = "Read data",
                    Type = ScopeType.Resource,
                    Emphasize = false,
                },
                new Scope
                {
                    Name = "write",
                    DisplayName = "Write data",
                    Type = ScopeType.Resource,
                    Emphasize = true,
                },
                new Scope()
                {
                    Name = "MyAccountApi",
                    DisplayName = "Access to MyAccount Api",
                    Type = ScopeType.Resource,
                    Emphasize = true,
                    Claims = new List<ScopeClaim>()
                  {
                    new ScopeClaim("customerid", true)
                  }
                }, 
             };
        }
    }
}
