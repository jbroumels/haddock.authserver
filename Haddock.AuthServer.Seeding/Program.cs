﻿using System.Collections.Generic;
using System.Linq;
using Haddock.AuthServer.Domain;
using Haddock.AuthServer.Domain.Repository;
using Haddock.AuthServer.Seeding.Config;
using IdentityServer3.Core.Models;
using IdentityServer3.EntityFramework;

namespace Haddock.AuthServer.Seeding
{
    class Program
    {
        static void Main(string[] args)
        {
            var efConfig = new EntityFrameworkServiceOptions
            {
                ConnectionString = "AuthServer",
            };

            // these two calls just pre-populate the test DB from the in-memory config
            ConfigureClients(Clients.Get(), efConfig);
            ConfigureScopes(Scopes.Get(), efConfig);
            ConfigureUserAuthenticators(ClientCustomLocalUserAuthentications.Get(), efConfig);
        }

        private static void ConfigureUserAuthenticators(IEnumerable<ClientCustomLocalUserAuthentication> authenticators, EntityFrameworkServiceOptions efConfig)
        {
            using (var db = new AuthServerContext())
            {
                if (!db.ClientCustomLocalUserAuthentications.Any())
                {
                    foreach (var a in authenticators)
                    {
                        db.ClientCustomLocalUserAuthentications.Add(a);
                    }
                    db.SaveChanges();
                }
            }
        }

        public static void ConfigureClients(IEnumerable<IdentityServer3.Core.Models.Client> clients, EntityFrameworkServiceOptions options)
        {
            using (var db = new ClientConfigurationDbContext(options.ConnectionString, options.Schema))
            {
                if (!db.Clients.Any())
                {
                    foreach (var c in clients)
                    {
                        var e = c.ToEntity();
                        db.Clients.Add(e);
                    }
                    db.SaveChanges();
                }
            }
        }

        public static void ConfigureScopes(IEnumerable<IdentityServer3.Core.Models.Scope> scopes, EntityFrameworkServiceOptions options)
        {
            using (var db = new ScopeConfigurationDbContext(options.ConnectionString, options.Schema))
            {
                if (!db.Scopes.Any())
                {
                    foreach (var s in scopes)
                    {
                        var e = s.ToEntity();
                        db.Scopes.Add(e);
                    }
                    db.SaveChanges();
                }
            }
        }
    }
}
