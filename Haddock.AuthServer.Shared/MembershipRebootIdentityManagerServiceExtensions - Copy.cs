using System.ComponentModel.DataAnnotations;
using BrockAllen.MembershipReboot;
using BrockAllen.MembershipReboot.Ef;
using BrockAllen.MembershipReboot.Relational;
using IdentityManager;
using IdentityManager.Configuration;
using IdentityManager.MembershipReboot;

namespace Haddock.AuthServer.AdminWebsite.Config
{
    public static class MembershipRebootIdentityManagerServiceExtensions
    {
        public static void Configure(this IdentityManagerServiceFactory factory, string connectionString, string connectionStringUsers)
        {
            //factory.IdentityManagerService = new IdentityManager.Configuration.Registration<IIdentityManagerService, MembershipRebootIdentityManagerService<RelationalUserAccount, RelationalGroup>>();
            
            //factory.IdentityManagerService = new Registration<IIdentityManagerService, CustomIdentityManagerService>();
            //factory.Register(new Registration<CustomUserAccountService>());
            //factory.Register(new Registration<CustomGroupService>());
            //factory.Register(new Registration<CustomUserRepository>());
            //factory.Register(new Registration<CustomGroupRepository>());
            //factory.Register(new Registration<CustomDatabase>(resolver => new CustomDatabase(connectionStringUsers)));
            //factory.Register(new Registration<CustomConfig>(CustomConfig.Config));

            factory.IdentityManagerService = new Registration<IIdentityManagerService, MembershipRebootIdentityManagerService<RelationalUserAccount, RelationalGroup>>();
            factory.Register(new Registration<UserAccountService<RelationalUserAccount>>());
            factory.Register(new Registration<GroupService<RelationalGroup>>());
            factory.Register(new Registration<DbContextUserAccountRepository<CustomDatabase, RelationalUserAccount>>());
            factory.Register(new Registration<DbContextGroupRepository<CustomDatabase, RelationalGroup>>());
            factory.Register(new Registration<CustomDatabase>(resolver => new CustomDatabase(connectionStringUsers)));
            factory.Register(new Registration<CustomConfig>(CustomConfig.Config));
        }
    }

    public class CustomGroupService : GroupService<RelationalGroup>
    {
        public CustomGroupService(DbContextGroupRepository<CustomDatabase, RelationalGroup> repo, CustomConfig config)
            : base(config.DefaultTenant, repo)
        {

        }
    }

    public class CustomConfig : MembershipRebootConfiguration<RelationalUserAccount>
    {
        public static readonly CustomConfig Config;

        static CustomConfig()
        {
            Config = new CustomConfig();
            Config.PasswordHashingIterationCount = 10000;
            Config.RequireAccountVerification = false;
            //config.EmailIsUsername = true;
        }
    }

    public class CustomDatabase : MembershipRebootDbContext<RelationalUserAccount, RelationalGroup>
    {
        public CustomDatabase(string name)
            : base(name)
        {
        }
    }

    /*
    public class CustomIdentityManagerService : MembershipRebootIdentityManagerService<CustomUser, CustomGroup>
    {
        public CustomIdentityManagerService(CustomUserAccountService userSvc, CustomGroupService groupSvc)
            : base(userSvc, groupSvc)
        {
        }
    }

    public class CustomUser : RelationalUserAccount
    {
        [Display(Name = "First Name")]
        public virtual string FirstName { get; set; }
        [Display(Name = "Last Name")]
        public virtual string LastName { get; set; }
        public virtual int? Age { get; set; }
    }

    public class CustomUserAccountService : UserAccountService<CustomUser>
    {
        public CustomUserAccountService(CustomConfig config, CustomUserRepository repo)
            : base(config, repo)
        {
        }
    }

    public class CustomUserRepository : DbContextUserAccountRepository<CustomDatabase, CustomUser>
    {
        public CustomUserRepository(CustomDatabase ctx)
            : base(ctx)
        {
        }
    }

    public class CustomDatabase : MembershipRebootDbContext<CustomUser, CustomGroup>
    {
        public CustomDatabase(string name)
            : base(name)
        {
        }
    }
    public class CustomConfig : MembershipRebootConfiguration<CustomUser>
    {
        public static readonly CustomConfig Config;

        static CustomConfig()
        {
            Config = new CustomConfig();
            Config.PasswordHashingIterationCount = 10000;
            Config.RequireAccountVerification = false;
            //config.EmailIsUsername = true;
        }
    }
    public class CustomGroup : RelationalGroup
    {
        public virtual string Description { get; set; }
    }
    
    public class CustomGroupService : GroupService<CustomGroup>
    {
        public CustomGroupService(CustomGroupRepository repo, CustomConfig config)
            : base(config.DefaultTenant, repo)
        {

        }
    }

    public class CustomGroupRepository : DbContextGroupRepository<CustomDatabase, CustomGroup>
    {
        public CustomGroupRepository(CustomDatabase ctx)
            : base(ctx)
        {
        }
    }*/
}