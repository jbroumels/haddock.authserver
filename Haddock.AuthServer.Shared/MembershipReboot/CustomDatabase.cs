using BrockAllen.MembershipReboot.Ef;

namespace Haddock.AuthServer.Shared.MembershipReboot
{
    public class CustomDatabase : MembershipRebootDbContext<CustomUser, CustomGroup>
    {
        public CustomDatabase()
            : base("AuthServerUsers")
        {
        }
    }
}