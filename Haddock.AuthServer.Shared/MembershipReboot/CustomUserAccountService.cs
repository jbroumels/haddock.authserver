using BrockAllen.MembershipReboot;

namespace Haddock.AuthServer.Shared.MembershipReboot
{
    public class CustomUserAccountService : UserAccountService<CustomUser>
    {
        public CustomUserAccountService(CustomConfig config, CustomUserRepository repo)
            : base(config, repo)
        {
        }
    }
}