using BrockAllen.MembershipReboot;

namespace Haddock.AuthServer.Shared.MembershipReboot
{
    public class CustomConfig : MembershipRebootConfiguration<CustomUser>
    {
        public CustomConfig()
        {
            PasswordHashingIterationCount = 10000;
            RequireAccountVerification = false;
            MultiTenant = false;
            EmailIsUsername = false;
            
        }

    }
}