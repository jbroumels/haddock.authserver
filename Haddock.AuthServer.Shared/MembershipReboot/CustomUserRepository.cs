using BrockAllen.MembershipReboot.Ef;

namespace Haddock.AuthServer.Shared.MembershipReboot
{
    public class CustomUserRepository : DbContextUserAccountRepository<CustomDatabase, CustomUser>
    {
        public CustomUserRepository(CustomDatabase ctx)
            : base(ctx)
        {
        }
    }
}