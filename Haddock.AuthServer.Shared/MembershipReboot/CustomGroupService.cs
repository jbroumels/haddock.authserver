using BrockAllen.MembershipReboot;

namespace Haddock.AuthServer.Shared.MembershipReboot
{
    public class CustomGroupService : GroupService<CustomGroup>
    {
        public CustomGroupService(CustomGroupRepository repo, CustomConfig config)
            : base(config.DefaultTenant, repo)
        {

        }
    }
}