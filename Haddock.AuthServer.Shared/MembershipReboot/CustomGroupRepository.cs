using BrockAllen.MembershipReboot.Ef;

namespace Haddock.AuthServer.Shared.MembershipReboot
{
    public class CustomGroupRepository : DbContextGroupRepository<CustomDatabase, CustomGroup>
    {
        public CustomGroupRepository(CustomDatabase ctx)
            : base(ctx)
        {
        }
    }
}