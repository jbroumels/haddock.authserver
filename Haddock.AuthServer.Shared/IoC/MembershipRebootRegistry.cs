﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BrockAllen.MembershipReboot;
using Enosis.Common.Ioc.Simple;
using Haddock.AuthServer.Shared.MembershipReboot;

namespace Haddock.AuthServer.Shared.IoC
{
    public class MembershipRebootRegistry : SimpleRegistry
    {
        public override void RunConfiguration(ISimpleRegistryImplementation implementation)
        {
            implementation.RegisterType<CustomUserRepository>();

            implementation.RegisterType<CustomUserAccountService>();
            implementation.RegisterType<CustomDatabase>();
            implementation.RegisterType<CustomGroupService>();
            
            implementation.RegisterType<CustomGroupRepository>();
            implementation.RegisterType<CustomConfig>();
        }
    }
}
