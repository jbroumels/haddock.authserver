﻿using Enosis.Common.Ioc;
using Enosis.Common.Ioc.SetterInjection;
using UnityConfiguration;

namespace Haddock.AuthServer.Shared.IoC
{
    public class HaddockRegistry : UnityRegistry
    {
        public HaddockRegistry()
        {
            Scan(PerformScan);
        }

        protected virtual void PerformScan(IAssemblyScanner x)
        {
            // Search for the registrations
            x.AssembliesInBaseDirectory(); // Use these assemblies
            x.IncludeNamespace("Haddock"); // Only use these namespaces
            x.WithNamingConvention(); // Register anything that uses the naming convention
            x.ExcludeType<EnosisRegistryForPerAssemblyRegistries>();
            // Exclude the registry that will scan other registries

            // here come our custom conventions, that are built in
            if (IocConfig.UseIObjectMapperConvention)
            {
                x.WithObjectMapperConvention();
            }

            // and custom extensions
            if (IocConfig.UseSetterInjectionExtension)
            {
                AddExtension<CustomDependencyAttributeExtension<SetterDependencyAttribute>>();
            }
        }
    }
}