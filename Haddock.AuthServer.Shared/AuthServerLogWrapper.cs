using System;
using Enosis.Common.Ioc.Simple;
using Enosis.Common.Logging;

namespace Haddock.AuthServer.Shared
{
    public class AuthServerLogWrapper : LoggingExtensions.Logging.ILog, LoggingExtensions.Logging.ILog<AuthServerLogWrapper>
    {
        public AuthServerLogWrapper()
        {
            int x =1;

        }

        private enum IdentityServerLogWrapperLogEvent
        {
            IdentityServer
        }

        public ISimpleResolver resolver;


        private ILogger _logger;

        public void InitializeFor(string loggerName)
        {
            this._logger = resolver.Resolve<ILogger>();
        }

        public void Debug(string message, params object[] formatting)
        {
          this._logger.LogDebug(IdentityServerLogWrapperLogEvent.IdentityServer, message, formatting);
        }

        public void Debug(Func<string> message)
        {
            this._logger.LogDebug(IdentityServerLogWrapperLogEvent.IdentityServer, message());
        }

        public void Info(string message, params object[] formatting)
        {
          this._logger.LogInformation(IdentityServerLogWrapperLogEvent.IdentityServer, message, formatting);
        }

        public void Info(Func<string> message)
        {
            this._logger.LogInformation(IdentityServerLogWrapperLogEvent.IdentityServer, message());
        }

        public void Warn(string message, params object[] formatting)
        {
            this._logger.LogWarning(IdentityServerLogWrapperLogEvent.IdentityServer, message, formatting);
        }

        public void Warn(Func<string> message)
        {
            this._logger.LogWarning(IdentityServerLogWrapperLogEvent.IdentityServer, message());
        }

        public void Error(string message, params object[] formatting)
        {
            this._logger.LogError(IdentityServerLogWrapperLogEvent.IdentityServer, message, formatting);
        }

        public void Error(Func<string> message)
        {
            this._logger.LogError(IdentityServerLogWrapperLogEvent.IdentityServer, message());
        }

        public void Error(Func<string> message, Exception exception)
        {
            this._logger.LogException(IdentityServerLogWrapperLogEvent.IdentityServer, exception, message());
        }

        public void Fatal(string message, params object[] formatting)
        {
            this._logger.LogException(IdentityServerLogWrapperLogEvent.IdentityServer, new Exception(string.Format(message, formatting)));
        }

        public void Fatal(Func<string> message)
        {
            this._logger.LogException(IdentityServerLogWrapperLogEvent.IdentityServer, new Exception(message()));
        }

        public void Fatal(Func<string> message, Exception exception)
        {
            this._logger.LogException(IdentityServerLogWrapperLogEvent.IdentityServer, exception, message());
        }
    }
}