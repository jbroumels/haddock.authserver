﻿-- Start try/catch
BEGIN TRY
	-- Start a new transaction
	BEGIN TRANSACTION

CREATE TABLE [dbo].[Clients] (
    [Id]                               INT             IDENTITY (1, 1) NOT NULL,
    [Enabled]                          BIT             NOT NULL,
    [ClientId]                         NVARCHAR (200)  NOT NULL,
    [ClientName]                       NVARCHAR (200)  NOT NULL,
    [ClientUri]                        NVARCHAR (2000) NULL,
    [LogoUri]                          NVARCHAR (MAX)  NULL,
    [RequireConsent]                   BIT             NOT NULL,
    [AllowRememberConsent]             BIT             NOT NULL,
    [Flow]                             INT             NOT NULL,
    [AllowClientCredentialsOnly]       BIT             NOT NULL,
    [AllowAccessToAllScopes]		   BIT			   NOT NULL,
	[IdentityTokenLifetime]            INT             NOT NULL,
    [AccessTokenLifetime]              INT             NOT NULL,
    [AuthorizationCodeLifetime]        INT             NOT NULL,
    [AbsoluteRefreshTokenLifetime]     INT             NOT NULL,
    [SlidingRefreshTokenLifetime]      INT             NOT NULL,
    [RefreshTokenUsage]                INT             NOT NULL,
    [UpdateAccessTokenOnRefresh]	   BIT             NOT NULL,
    [RefreshTokenExpiration]           INT             NOT NULL,
    [AccessTokenType]                  INT             NOT NULL,
    [EnableLocalLogin]                 BIT             NOT NULL,
    [IncludeJwtId]                     BIT             NOT NULL,
    [AlwaysSendClientClaims]           BIT             NOT NULL,
    [PrefixClientClaims]               BIT             NOT NULL,
	[AllowAccessToAllGrantTypes]	   BIT			   NOT NULL,
    CONSTRAINT [PK_dbo.Clients] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [UNQ_ClientId] UNIQUE NONCLUSTERED ([ClientId] ASC)
);


CREATE TABLE [dbo].[ClientCorsOrigins] (
    [Id]        INT            IDENTITY (1, 1) NOT NULL,
    [Origin]    NVARCHAR (150) NOT NULL,
    [Client_Id] INT            NULL,
    CONSTRAINT [PK_dbo.ClientCorsOrigins] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.ClientCorsOrigins_dbo.Clients_Client_Id] FOREIGN KEY ([Client_Id]) REFERENCES [dbo].[Clients] ([Id])
);


CREATE NONCLUSTERED INDEX [IX_Client_Id]
    ON [dbo].[ClientCorsOrigins]([Client_Id] ASC);


CREATE TABLE [dbo].[ClientClaims] (
    [Id]        INT            IDENTITY (1, 1) NOT NULL,
    [Type]      NVARCHAR (250) NOT NULL,
    [Value]     NVARCHAR (250) NOT NULL,
    [Client_Id] INT            NULL,
    CONSTRAINT [PK_dbo.ClientClaims] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.ClientClaims_dbo.Clients_Client_Id] FOREIGN KEY ([Client_Id]) REFERENCES [dbo].[Clients] ([Id])
);


CREATE NONCLUSTERED INDEX [IX_Client_Id]
    ON [dbo].[ClientClaims]([Client_Id] ASC);


CREATE TABLE [dbo].[ClientSecrets] (
    [Id]               INT                IDENTITY (1, 1) NOT NULL,
    [Value]            NVARCHAR (250)     NOT NULL,
    [Type]			   NVARCHAR (250)     NULL,
    [Description]      NVARCHAR (2000)    NULL,
    [Expiration]       DATETIMEOFFSET (7) NULL,
    [Client_Id]        INT                NULL,
    CONSTRAINT [PK_dbo.ClientSecrets] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.ClientSecrets_dbo.Clients_Client_Id] FOREIGN KEY ([Client_Id]) REFERENCES [dbo].[Clients] ([Id])
);

CREATE NONCLUSTERED INDEX [IX_Client_Id]
    ON [dbo].[ClientSecrets]([Client_Id] ASC);



CREATE TABLE [dbo].[ClientGrantTypeRestrictions] (
    [Id]        INT            IDENTITY (1, 1) NOT NULL,
    [GrantType] NVARCHAR (250) NOT NULL,
    [Client_Id] INT            NULL,
    CONSTRAINT [PK_dbo.ClientGrantTypeRestrictions] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.ClientGrantTypeRestrictions_dbo.Clients_Client_Id] FOREIGN KEY ([Client_Id]) REFERENCES [dbo].[Clients] ([Id])
);


CREATE NONCLUSTERED INDEX [IX_Client_Id]
    ON [dbo].[ClientGrantTypeRestrictions]([Client_Id] ASC);


CREATE TABLE [dbo].[ClientIdPRestrictions] (
    [Id]        INT            IDENTITY (1, 1) NOT NULL,
    [Provider]  NVARCHAR (200) NOT NULL,
    [Client_Id] INT            NULL,
    CONSTRAINT [PK_dbo.ClientIdPRestrictions] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.ClientIdPRestrictions_dbo.Clients_Client_Id] FOREIGN KEY ([Client_Id]) REFERENCES [dbo].[Clients] ([Id])
);

CREATE NONCLUSTERED INDEX [IX_Client_Id]
    ON [dbo].[ClientIdPRestrictions]([Client_Id] ASC);



	CREATE TABLE [dbo].[ClientPostLogoutRedirectUris] (
    [Id]        INT             IDENTITY (1, 1) NOT NULL,
    [Uri]       NVARCHAR (2000) NOT NULL,
    [Client_Id] INT             NULL,
    CONSTRAINT [PK_dbo.ClientPostLogoutRedirectUris] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.ClientPostLogoutRedirectUris_dbo.Clients_Client_Id] FOREIGN KEY ([Client_Id]) REFERENCES [dbo].[Clients] ([Id])
);

CREATE NONCLUSTERED INDEX [IX_Client_Id]
    ON [dbo].[ClientPostLogoutRedirectUris]([Client_Id] ASC);



	CREATE TABLE [dbo].[ClientRedirectUris] (
    [Id]        INT             IDENTITY (1, 1) NOT NULL,
    [Uri]       NVARCHAR (2000) NOT NULL,
    [Client_Id] INT             NULL,
    CONSTRAINT [PK_dbo.ClientRedirectUris] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.ClientRedirectUris_dbo.Clients_Client_Id] FOREIGN KEY ([Client_Id]) REFERENCES [dbo].[Clients] ([Id])
);

CREATE NONCLUSTERED INDEX [IX_Client_Id]
    ON [dbo].[ClientRedirectUris]([Client_Id] ASC);


CREATE TABLE [dbo].[ClientScopeRestrictions] (
    [Id]        INT            IDENTITY (1, 1) NOT NULL,
    [Scope]     NVARCHAR (200) NOT NULL,
    [Client_Id] INT            NULL,
    CONSTRAINT [PK_dbo.ClientScopeRestrictions] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.ClientScopeRestrictions_dbo.Clients_Client_Id] FOREIGN KEY ([Client_Id]) REFERENCES [dbo].[Clients] ([Id])
);


CREATE NONCLUSTERED INDEX [IX_Client_Id]
    ON [dbo].[ClientScopeRestrictions]([Client_Id] ASC);



	CREATE TABLE [dbo].[Scopes] (
    [Id]                      INT             IDENTITY (1, 1) NOT NULL,
    [Enabled]                 BIT             NOT NULL,
    [Name]                    NVARCHAR (200)  NOT NULL,
    [DisplayName]             NVARCHAR (200)  NULL,
    [Description]             NVARCHAR (1000) NULL,
    [Required]                BIT             NOT NULL,
    [Emphasize]               BIT             NOT NULL,
    [Type]                    INT             NOT NULL,
    [IncludeAllClaimsForUser] BIT             NOT NULL,
    [ClaimsRule]              NVARCHAR (200)  NULL,
    [ShowInDiscoveryDocument] BIT             NOT NULL,
    CONSTRAINT [PK_dbo.Scopes] PRIMARY KEY CLUSTERED ([Id] ASC)
);



CREATE TABLE [dbo].[ScopeClaims] (
    [Id]                     INT             IDENTITY (1, 1) NOT NULL,
    [Name]                   NVARCHAR (200)  NOT NULL,
    [Description]            NVARCHAR (1000) NULL,
    [AlwaysIncludeInIdToken] BIT             NOT NULL,
    [Scope_Id]               INT             NULL,
    CONSTRAINT [PK_dbo.ScopeClaims] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.ScopeClaims_dbo.Scopes_Scope_Id] FOREIGN KEY ([Scope_Id]) REFERENCES [dbo].[Scopes] ([Id])
);

CREATE NONCLUSTERED INDEX [IX_Scope_Id]
    ON [dbo].[ScopeClaims]([Scope_Id] ASC);



	-- Catch if the statements fail, commit otherwise
	COMMIT
END TRY
BEGIN CATCH
	--PRINT 'Found error, rolling back'
	ROLLBACK
    
    DECLARE @errorText nvarchar(max)
    SET @errorText = 'Found error: #' + CONVERT(nvarchar(max), ERROR_NUMBER())
    + ' / Line #' + CONVERT(nvarchar(max), ERROR_LINE())
    + ' / Message: ' + CONVERT(nvarchar(max), ERROR_MESSAGE())
    
    RAISERROR (@errorText, -- Message text.
           16, -- Severity,
           1); -- State
	
END CATCH