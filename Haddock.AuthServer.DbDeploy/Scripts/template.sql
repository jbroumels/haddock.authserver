﻿-- Start try/catch
BEGIN TRY
	-- Start a new transaction
	BEGIN TRANSACTION

	-- Catch if the statements fail, commit otherwise
	COMMIT
END TRY
BEGIN CATCH
	--PRINT 'Found error, rolling back'
	ROLLBACK
    
    DECLARE @errorText nvarchar(max)
    SET @errorText = 'Found error: #' + CONVERT(nvarchar(max), ERROR_NUMBER())
    + ' / Line #' + CONVERT(nvarchar(max), ERROR_LINE())
    + ' / Message: ' + CONVERT(nvarchar(max), ERROR_MESSAGE())
    
    RAISERROR (@errorText, -- Message text.
           16, -- Severity,
           1); -- State
	
END CATCH