﻿

-- Start try/catch
BEGIN TRY
	-- Start a new transaction
	BEGIN TRANSACTION

	CREATE TABLE dbo.ClientCustomLocalUserAuthentications
	(
	Id int NOT NULL IDENTITY (1, 1),
	ClientId nvarchar(200) NOT NULL,
	LocalUserAuthenticatorType int NOT NULL
	)  ON [PRIMARY]

ALTER TABLE dbo.ClientCustomLocalUserAuthentications ADD CONSTRAINT
	PK_ClientCustomLocalUserAuthentications PRIMARY KEY CLUSTERED 
	(
	Id
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]


CREATE NONCLUSTERED INDEX IX_ClientCustomLocalUserAuthentications ON dbo.ClientCustomLocalUserAuthentications
	(
	ClientId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

	-- Catch if the statements fail, commit otherwise
	COMMIT
END TRY
BEGIN CATCH
	--PRINT 'Found error, rolling back'
	ROLLBACK
    
    DECLARE @errorText nvarchar(max)
    SET @errorText = 'Found error: #' + CONVERT(nvarchar(max), ERROR_NUMBER())
    + ' / Line #' + CONVERT(nvarchar(max), ERROR_LINE())
    + ' / Message: ' + CONVERT(nvarchar(max), ERROR_MESSAGE())
    
    RAISERROR (@errorText, -- Message text.
           16, -- Severity,
           1); -- State
	
END CATCH