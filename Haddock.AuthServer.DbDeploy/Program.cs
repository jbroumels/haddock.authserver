﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using DbUp;

namespace Haddock.AuthServer.DbDeploy
{
    [ExcludeFromCodeCoverage]
    class Program
    {
        public static int Main(string[] args)
        {
            string upgradeMessage;

            bool upgradeResultDocuments = UpgradeDb(out upgradeMessage);
            if (!upgradeResultDocuments)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(upgradeMessage);
                Console.ResetColor();
                System.Threading.Thread.Sleep(2000); // visible in screen when running locally
                return -1;
            }

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(upgradeMessage);
            Console.ResetColor();

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("All databases updated. Success!!");
            Console.ResetColor();

            System.Threading.Thread.Sleep(2000); // visible in screen when running locally
            return 0;
        }


        private static bool UpgradeDb(out string upgradeMessage)
        {
            var connectionString = ConfigurationManager.ConnectionStrings["AuthServer"].ConnectionString;

            var upgrader =
                DeployChanges.To
                    .SqlDatabase(connectionString)
                    .WithScriptsEmbeddedInAssembly(Assembly.GetExecutingAssembly(), it => it.Contains(".Scripts.AuthServer."))
                    .WithoutTransaction()
                    .LogToConsole()
                    .Build();

            var result = upgrader.PerformUpgrade();

            if (!result.Successful)
            {
                upgradeMessage = result.Error.ToString();
                return false;
            }

            upgradeMessage = "AuthServer database upgraded";
            return true;
        }
    }
}
